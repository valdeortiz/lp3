# SOCCER CLUB ADMINISTRACION APP. 🚀

[![Java versions](https://img.shields.io/badge/JAVA-v8-blue)](https://www.java.com/es/download/) [![version](https://img.shields.io/badge/Version-v1.0-blue)](https://github.com/valdeortiz/SoccerClubWebApp)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a460577315c6434a96a79218ef4858fd)](https://www.codacy.com?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=valdeortiz/SoccerClubWebApp&amp;utm_campaign=Badge_Grade)
*** 
## Descarga de Repositorio :arrow_backward:

Ejecutar:``` git clone https://github.com/valdeortiz/SoccerClubWebApp.git ```

o decargar en el repositorio. En la seccion de clone, click en ***Descargar en zip***

### Importar al STS.
En la pestaña file, luego la opcion de importar y elegimos existing Meaven Project y finalizamos escogiendo la carpeta donde descargamos el repositorio.

***
## Pre-requisitos 📋.
Se necesita tener instalado Java 8 junto con spring. Recomendamos el uso de Spring tools IDE.
- [Java Download](https://www.java.com/es/download/)
- [Spring Tools](https://spring.io/tools)
- [Proyecto Spring Sin IDE(En caso de no usar STS)](https://hackerdev.net/viewtopic.php?t=7)



Seguir las intruciones del enunciado anterior para la Descarga del repositorio.


### Ejecucion 🔩
Ejecutamos el siguiente comando en el directorio ***app*** para compilar y levantar nuestro servidor.
> ```$ ./mvnw clean package -DskipTests -Dmaven.javadoc.skip=true -B -V```

> ```$ java -jar target/democlub-0.0.1-SNAPSHOT.jar ```

***

## Construido con 🛠️

- [Java](https://www.java.com/es/download/ "Pagina oficial del lenguaje de programacion Java")
- [Spring](https://spring.io/ "Framework Spring")


## Observacion 📢 

> ***Contruido con Sring Tools. En caso de no usar recomiendo leer el siguiente enlace***
[Proyecto Spring Sin IDE](https://hackerdev.net/viewtopic.php?t=7)

> ***Si al ejecutar `java --version`, no retorna un valor. Asegurate de tener descargado y vuelve a ejecutar `java --version` hasta que devuelva una version.***

> ***En la carpeta scripts se encuentra un script para la ejecucion***

## Documentacion oficial 📄
[JavaDoc](https://docs.oracle.com/javase/8/docs/technotes/tools/windows/javadoc.html "Documentacion oficial de java")

***

## Recomendaciones 📦

- Aegurarse de tener todo descargado correctamente.
- Tener conocimientos basicos en el protocolo http.
- Asegurarse de tener todo lo necesario instalado en su dispositivo, recomendamos la lectura de pre-requisitos.

## Autores ✒️

* **Valdemar Ortiz** - [valdeortiz](https://github.com/valdeortiz)
* **Marcelo Baez** -  [MBaez](https://github.com/Mbaez97)